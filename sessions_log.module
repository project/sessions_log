<?php

/**
 * Implementation of hook_perm().
 */
function sessions_log_perm() {
  return array('access sessions log');
}

/**
 * Implementation of hook_menu().
 */
function sessions_log_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/logs/sessions_log',
      'title' => t('Sessions log'),
      'description' => t('View user sessions log.'),
      'callback' => 'sessions_log_page',
      'access' => user_access('access sessions log'),
      'type' => MENU_NORMAL_ITEM,
    );
    $items[] = array(
      'path' => 'admin/logs/sessions_log_settings',
      'title' => t('Sessions log settings'),
      'description' => t('Control how your site logs sessions.'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('sessions_log_admin_settings'),
      'access' => user_access('administer site configuration'),
      'type' => MENU_NORMAL_ITEM,
    );
  }

  return $items;
}

/**
 * Implementation of hook_cron().
 *
 * Log data on current sessions.
 */
function sessions_log_cron() {
  $interval = time() - variable_get('sessions_log_last', 0);
  if ($interval > variable_get('sessions_log_interval', 3600)) {
    sessions_log_add();
    variable_get('sessions_log_last', time());
  }
}

/**
 * Add a row to the sessions log.
 */
function sessions_log_add() {
  $time = time();
  $timestamp = $time - variable_get('sessions_log_interval', 3600);
  $anonymous_count = sess_count($timestamp);
  $authenticated_count = sess_count($timestamp, FALSE);
  db_query("INSERT INTO {sessions_log} (timestamp, anonymous_count, authenticated_count) VALUES (%d, %d, %d)", $timestamp, $anonymous_count, $authenticated_count);
}

/**
 * Menu callback: present a sortable table of sessions log data.
 */
function sessions_log_page() {
  global $user;

  $sql = 'SELECT timestamp, anonymous_count, authenticated_count, (anonymous_count + authenticated_count) AS total_count FROM {sessions_log}';
  $header = array(
    array('data' => t('Time'), 'field' => 'timestamp', 'sort' => 'asc'),
    array('data' => t('Anonymous users'), 'field' => 'anonymous_count'),
    array('data' => t('Authanticated users'), 'field' => 'authenticated_count'),
    array('data' => t('Total users'), 'field' => 'total_count'),
  );

  $sql .= tablesort_sql($header);

  $result = pager_query($sql, 50, 0, NULL, $user->uid);

  while ($record = db_fetch_object($result)) {

    $rows[] = array(
      format_date($record->timestamp),
      $record->anonymous_count,
      $record->authenticated_count,
      $record->total_count,
    );

  }

  if (!$rows) {
    $rows[] = array(array('data' => t('No logged sessions found.'), 'colspan' => '4'));
  }

  $output = theme('table', $header, $rows);
  $output .= theme('pager', NULL, 50, 0);
  return $output;
}

/**
 * Generate an administrative settings form for configuring sessions logs.
 */
function sessions_log_admin_settings() {
  $form = array();
  $form['sessions_log_interval'] = array(
    '#type' => 'select',
    '#title' => t('Sessions log interval'),
    '#description' => t('Select a period of time to use for session logging. Statistics will be generated based on the number of users who have accessed the site in the last period of this amount of time.'),
    '#options' => drupal_map_assoc(array(600, 900, 1800, 2700, 3600, 5400, 7200, 10800, 21600, 43200, 86400), 'format_interval'),
    '#default_value' => variable_get('sessions_log_interval', 3600),
  );
  return system_settings_form($form);
}

/**
 * Implementation of hook_graphstat().
 */
function sessions_log_graphstat() {
  $graphs = array();

  $graphs['sessions_log'] = array(
    'title' => t('Sessions'),
    'graph_sessions_all' => array(
      'title' => t('Number of Sessions'),
      'xlabel' => t('Date (MM/DD/YYYY - HH:MM)'),
      'ylabel' => t('# Sessions'),
      'legend' => array(t('anonymous'), t('authenticated'), t('total')),
      'data' => array(sessions_log_graph_data('anonymous_count'), sessions_log_graph_data('authenticated_count'), sessions_log_graph_data('(anonymous_count + authenticated_count)')),
    ),
  );

  return $graphs;
}

/**
 * Graphing for sessions data.
 */
function sessions_log_graph_data($key) {
  $data = array();

  $timestamp = db_result(db_query('SELECT MIN(timestamp) FROM {sessions_log}'));
  if ($timestamp) {
    // one point per week (at least 15 points)
    if (!$points) {
      $points = max((time() - $timestamp) / (60*60*24*7), 15);
      while ($points > 30) {
        $points /= 2;
      }
    }
    $interval = round((time() - $timestamp) / ($points - 1), 0);
    $time = $timestamp;
    for ($i = 0; $i < $points; $i++) {
      $data[format_date($time)] = db_result(db_query('SELECT AVG('. $key .') FROM {sessions_log} WHERE timestamp >= %d AND timestamp < %d', $time, $time + $interval));
      $time += $interval;
    }
  }

  return $data;
}

